//////////////////////////////////////////////////////////////////////////
// Copyright (c) 1999, 2000, 2001, 2002  Adel I. Mirzazhanov
// Copyright (c) 2020  Boris Popov <popov@whitekefir.ru>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  1. Redistributions of source code must retain the above
//     copyright notice, this list of conditions and the
//     following disclaimer.
//
//  2. Redistributions in binary form must reproduce the
//     above copyright notice, this list of conditions and the
//     following disclaimer in the documentation and/or other
//     materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names
//     of its contributors may be used to endorse or promote
//     products derived from this software without specific
//     prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#ifndef _OWNTYPES_H_
#define _OWNTYPES_H_

typedef unsigned int   pass_mode_t; // password generation mode
typedef unsigned int   UINT;
typedef unsigned short USHORT;
typedef short int      SHORT;
typedef int            boolean;
typedef uint32_t       UINT32;

#define TRUE  1
#define FALSE 0

#define PPG_MAX_PASSWORD_LENGTH 255

#endif // _OWNTYPES_H_
