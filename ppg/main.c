//////////////////////////////////////////////////////////////////////////
// Copyright (c) 1999, 2000, 2001, 2002  Adel I. Mirzazhanov
// Copyright (c) 2020  Boris Popov <popov@whitekefir.ru>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  1. Redistributions of source code must retain the above
//     copyright notice, this list of conditions and the
//     following disclaimer.
//
//  2. Redistributions in binary form must reproduce the
//     above copyright notice, this list of conditions and the
//     following disclaimer in the documentation and/or other
//     materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names
//     of its contributors may be used to endorse or promote
//     products derived from this software without specific
//     prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)
#include <strings.h>
#endif
#include <string.h>
#include <time.h>

#ifdef __NetBSD__
#include <unistd.h>
#endif

#define MAX_MODE_LENGTH       4
#define DEFAULT_MIN_PASS_LEN  8
#define DEFAULT_MAX_PASS_LEN 10
#define DEFAULT_NUM_OF_PASS   6

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE
#endif

#ifndef __NetBSD__
#include <unistd.h>
#endif

#include <ppg/ppg.h>
#include <ppg/restrict.h>
#include <ppg/smbl_type.h>
#include <ppg/errs.h>
#include <ppg/convert.h>
#include <ppg/rnd.h>
#include <ppg/version.h>
#include "getopt.h"


extern char* ppg_optarg; // global argument pointer

UINT32 get_user_seq(void);
UINT32 com_line_user_seq(char* seq);
void print_help();
void checkopt(char* opt);
int construct_mode(char* str_mode, pass_mode_t* mde);

int main(int argc, char* argv[]) {
     int i = 0;
     char* pass_string;
     char* hyph_pass_string;
     time_t tme;

     int option = 0;                   // programm option
     int exclude_list_present = FALSE; // exclude list present
     int quiet_present = FALSE;        // quiet mode flag
     int hyph_req_present = FALSE;     // request to print hyphenated password

     pass_mode_t mode = 0;
     unsigned int pass_mode_present = FALSE;        // password generation mode flag
     USHORT min_pass_length = DEFAULT_MIN_PASS_LEN; // min password length
     USHORT max_pass_length = DEFAULT_MAX_PASS_LEN; // max password length

     int number_of_pass = DEFAULT_NUM_OF_PASS; // number of passwords to generate
     UINT32 user_defined_seed = 0L;            // user defined random seed
     int user_defined_seed_present = FALSE;    // user defined random seed flag
     char* str_mode;                           // string mode pointer
     char* com_line_seq;
     char* spell_pass_string = NULL;
     int spell_present = FALSE;                // spell password mode flag
     unsigned int delimiter_flag_present = FALSE;

     //
     // Analize options
     //
     while ((option = ppg_getopt(argc, argv, "M:E:sdc:n:m:x:htvlq")) != -1) {
	  switch (option) {
	  case 'M': // mode parameter
	       str_mode = ppg_optarg;
	       if((construct_mode(str_mode, &mode)) == -1)
		    err_app_fatal("construct_mode",
				  "wrong parameter");
	       pass_mode_present = TRUE;
	       break;
	  case 'E': // exclude char
	       if(set_exclude_list(ppg_optarg) == -1)
		    err_app_fatal("set_exclude_list",
				  "string is too long (max. 93 characters)");
	       exclude_list_present = TRUE;
	       break;

	  case 'l':
	       spell_present = TRUE;
	       break;
#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)
	  case 's': // user random seed required
	       user_defined_seed = get_user_seq();
	       user_defined_seed_present = TRUE;
	       break;
#endif // WIN32
	  case 'c': // user random seed given in command line
	       com_line_seq = ppg_optarg;
	       user_defined_seed = com_line_user_seq(com_line_seq);
	       user_defined_seed_present = TRUE;
	       break;
	  case 'd': // no delimiters option
	       delimiter_flag_present = TRUE;
	       break;
	  case 'q': // quiet mode
	       quiet_present = TRUE;
	       break;

	  case 'n': // number of password specification
	       checkopt(ppg_optarg);
	       number_of_pass = atoi(ppg_optarg);
	       break;
	  case 'm': // min password length
	       checkopt(ppg_optarg);
	       min_pass_length = (USHORT) atoi(ppg_optarg);
	       break;
	  case 'x': // max password length
	       checkopt(ppg_optarg);
	       max_pass_length = (USHORT) atoi(ppg_optarg);
	       break;
	  case 't': // request to print hyphenated password
	       hyph_req_present = TRUE;
	       break;

	  case 'h': // print help
	       print_help();
	       return 0;

	  case 'v': // print version
	       printf("ppg (Pronunciation Password Generator)");
	       printf("\nversion %s", PPG_VERSION);
	       printf("\nCopyright (c) 1999 - 2003 Adel I. Mirzazhanov\n");
	       printf("Copyright (c) 2020 Boris Popov <popov@whitekefir.ru>\n");
	       return 0;

	  default: // print help end exit
	       print_help();
	       exit(-1);
	  }
     }
     if (pass_mode_present != TRUE)
	  mode = S_SS | S_NB | S_CL | S_SL;
     if (exclude_list_present == TRUE)
	  mode = mode | S_RS;
     if((tme = time(NULL)) == ((time_t) -1))
	  err_sys("time");
     if (user_defined_seed_present != TRUE)
	  x917_setseed((UINT32)tme, quiet_present);
     else
	  x917_setseed(user_defined_seed ^ (UINT32)tme, quiet_present);
     if (min_pass_length > max_pass_length)
	  max_pass_length = min_pass_length;

     // reserv space for password and hyphenated password and report of errors
     // 18 because the maximum length of element for hyphenated password is 17
     if ((pass_string = (char*) calloc(1, (size_t) (max_pass_length + 1))) == NULL ||
	 (hyph_pass_string = (char*) calloc(1, (size_t) (max_pass_length * 18))) == NULL)
	  err_sys_fatal("calloc");

     while (i < number_of_pass) {
	  if (gen_pron_pass(pass_string, hyph_pass_string,
			    min_pass_length, max_pass_length, mode) == -1) {
	       err_app_fatal("ppg", "wrong password length parameter");
	  }

	  fprintf (stdout, "%s", pass_string);
	  if (hyph_req_present == TRUE)
	       fprintf(stdout, " (%s)", hyph_pass_string);
	  if (spell_present == TRUE) {
	       spell_pass_string = spell_word(pass_string, spell_pass_string);
	       fprintf(stdout, (" %s"), spell_pass_string);
	       free((void*) spell_pass_string);
	  }
	  if (delimiter_flag_present == FALSE)
	       fprintf(stdout, "\n");
	  fflush (stdout);
	  i++;
     } // end of while
     free((void*) pass_string);
     free((void*) hyph_pass_string);
     return 0;
} // end of main

#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)

// Routine that gets user random sequense
UINT32 get_user_seq() {
     char* seq;
     UINT32 prom[2] = { 0L, 0L };
     UINT32 sdres = 0L;
     printf("\nPlease enter some random data (only first %lu are significant)\n", sizeof(prom));
     seq = (char*) getpass("(eg. your old password):>");
     if (strlen(seq) < sizeof(prom))
	  (void) memcpy((void*) &prom[0], (void*) seq, (int) strlen(seq));
     else
	  (void) memcpy((void*) &prom[0], (void*) seq, sizeof(prom));
     sdres = prom[0] ^ prom[1];
     return sdres;
}
#endif // WIN32

// Routine that gets user random sequense
UINT32 com_line_user_seq(char* seq) {
     UINT32 prom[2] = { 0L, 0L };
     UINT32 sdres = 0L;
     if (strlen(seq) < sizeof (prom))
	  (void) memcpy((void*) &prom[0], (void*) seq, (int) strlen(seq));
     else
	  (void) memcpy((void*) &prom[0], (void*) seq, sizeof(prom));
     sdres = prom[0]^prom[1];
     return (sdres);
}

void print_help() {
     printf("ppg Pronunciation Password Generator\n");
     printf("        Copyright (c) Adel I. Mirzazhanov\n");
     printf("        Copyright (c) Boris Popov <popov@whitekefir.ru>\n");
     printf("\nppg \n");
     printf("      [-M mode] [-E char_string] [-n num_of_pass] [-m min_pass_len]\n");
     printf("      [-x max_pass_len] [-c cl_seed] [-d] [-s] [-h] [-y] [-q]\n");
     printf("\n-M mode         new style password modes\n");
     printf("-E char_string  exclude characters from password generation process\n");
     printf("-n num_of_pass  generate num_of_pass passwords\n");
     printf("-m min_pass_len minimum password length\n");
     printf("-x max_pass_len maximum password length\n");
#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)
     printf("-s              ask user for a random seed for password\n");
     printf("                generation\n");
#endif // WIN32
     printf("-c cl_seed      use cl_seed as a random seed for password\n");
     printf("-d              do NOT use any delimiters between generated passwords\n");
     printf("-l              spell generated password\n");
     printf("-t              print pronunciation for generated pronounceable password\n");
     printf("-q              quiet mode (do not print warnings)\n");
     printf("-h              print this help screen\n");
     printf("-v              print version information\n");
}

void checkopt(char* opt) {
     for (unsigned int i = 0; i < strlen(opt); i++) {
	  if (opt[i] != '0' &&
	      opt[i] != '1' &&
	      opt[i] != '2' &&
	      opt[i] != '3' &&
	      opt[i] != '4' &&
	      opt[i] != '5' &&
	      opt[i] != '6' &&
	      opt[i] != '7' &&
	      opt[i] != '8' &&
	      opt[i] != '9')
	       err_app_fatal("checkopt", "wrong option format");
     }
}

int construct_mode(char* s_mode, pass_mode_t* mde) {
     pass_mode_t mode = 0;
     int ch = 0;
     int i = 0;
     int str_length = 0;

     str_length = strlen(s_mode);

     if (str_length > MAX_MODE_LENGTH) return -1;
     for (i = 0; i < str_length; i++) {
	  ch = (int) *s_mode;
	  switch(ch) {
	  case 's':
	       mode = mode | S_SS;
	       break;
	  case 'n':
	       mode = mode | S_NB;
	       break;
	  case 'c':
	       mode = mode | S_CL;
	       break;
	  case 'l':
	       mode = mode | S_SL;
	       break;
	  default:
	       return -1;
	       break;
	  }
	  s_mode++;
     }
     *mde = mode;
     return 0;
}
