//////////////////////////////////////////////////////////////////////////
// Copyright (c) 1999, 2000, 2001, 2002  Adel I. Mirzazhanov
// Copyright (c) 2020  Boris Popov <popov@whitekefir.ru>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  1. Redistributions of source code must retain the above
//     copyright notice, this list of conditions and the
//     following disclaimer.
//
//  2. Redistributions in binary form must reproduce the
//     above copyright notice, this list of conditions and the
//     following disclaimer in the documentation and/or other
//     materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names
//     of its contributors may be used to endorse or promote
//     products derived from this software without specific
//     prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "getopt.h"

static int badopt(const char* mess, int ch);

char* ppg_optarg;          // Global argument pointer
static int ppg_optind = 0; // Global argv index
static int ppg_opterr = 1; // for compatibility, should error be printed?
static int ppg_optopt;     // for compatibility, option character checked 
static char* scan = NULL;
static const char* prog = "ppg";

static int badopt(const char* mess, int ch) {
     if (ppg_opterr) {
	  fprintf(stderr,"%s%s%c\n", prog, mess, ch);
	  fflush(stderr);
     }
     return ('?');
}

int ppg_getopt(int argc, char* argv[], const char* optstring) {
     char c;
     const char* place;
     prog = argv[0];
     ppg_optarg = NULL;

     if (ppg_optind == 0) {
	  scan = NULL;
	  ppg_optind++;
     }

     if (scan == NULL || *scan == '\0') {
	  if (ppg_optind >= argc         ||
	      argv[ppg_optind][0] != '-' ||
	      argv[ppg_optind][1] == '\0') {
	       return (EOF);
	  }
	  if (argv[ppg_optind][1] == '-' &&
	      argv[ppg_optind][2] == '\0') {
	       ppg_optind++;
	       return (EOF);
	  }
	  scan = argv[ppg_optind++] + 1;
     }

     c = *scan++;
     ppg_optopt = c & 0377;
     for (place = optstring; place != NULL && *place != '\0'; ++place)
	  if (*place == c) break;

     if (place == NULL || *place == '\0' || c == ':' || c == '?') {
	  return (badopt(": unknown option -", c));
     }

     place++;
     if (*place == ':') {
	  if (*scan != '\0') {
	       ppg_optarg = scan;
	       scan = NULL;
	  } else if (ppg_optind >= argc) {
	       return (badopt(": option requires an argument -", c));
	  } else {
	       ppg_optarg = argv[ppg_optind++];
	  }
     }
     return (c & 0377);
}
