#include "minunit.h"
#include "src/endian.h"

void test_setup() {
     //
     //TODO
     //
}

void test_teardown() {
     //
     //TODO
     //
}

MU_TEST(endian_00) {
     if (endian != endian_UNDEF)
	  mu_fail("before detect endian!");
}

MU_TEST(endian_01) {
     detect_endian();
     if (endian == endian_UNDEF)
	  mu_fail("after detect endian!");
}

//MU_TEST(endian_02) {
//     mu_check(little_endian());
//}

MU_TEST_SUITE(test_suite) {
     MU_SUITE_CONFIGURE(&test_setup, &test_teardown);
     MU_RUN_TEST(endian_00);
     MU_RUN_TEST(endian_01);
     //MU_RUN_TEST(endian_02);
}

int main() {
     MU_RUN_SUITE(test_suite);
     MU_REPORT();
     return MU_EXIT_CODE;
}
