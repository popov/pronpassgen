ROOT     := .
IDIR     := $(ROOT)/include
LIB_BD   := build_lib
PPG_BD   := build_ppg

VER_FILE := $(ROOT)/src/version.c
VERSION  := $(shell sed -e '/VERSION/!d' -e 's/[^0-9.]*\([0-9.]*\).*/\1/' $(VER_FILE))

A_NAME   := $(LIB_BD)/libppg.a
SO_NAME  := $(LIB_BD)/libppg.so
PPG_NAME := $(PPG_BD)/ppg
PPG-W-SO := $(PPG_BD)/ppg-w-so

prefix     := /usr/local
bindir     := $(prefix)/bin
libdir     := $(prefix)/lib
includedir := $(prefix)/include
INSTALL    := /usr/bin/install

S_PPG  :=              \
          ppg/main.c   \
          ppg/getopt.c

O_PPG := $(S_PPG:%.c=%.o)
O_PPG := $(notdir $(O_PPG))
O_PPG := $(addprefix $(PPG_BD)/, $(O_PPG))
D_PPG := $(O_PPG:%.o=%.d)

S_LIB     :=                  \
             src/convert.c    \
             src/errors.c     \
             src/pronpass.c   \
             src/randsymbol.c \
             src/restrict.c   \
             src/version.c    \
             src/rnd.c        \
             src/smbl.c       \
             src/endian.c

CFLAGS := -O2 -pipe -fPIC
LIBS   :=

$(shell pkgconf --exists openssl)
ifeq ($(.SHELLSTATUS),0)
  CFLAGS += -DHAVE_OPENSSL
  CFLAGS += $(shell pkgconf --cflags openssl)
  LIBS   += $(shell pkgconf --libs openssl)
else
  S_LIB += src/sha.c
endif

CFLAGS   += -I $(IDIR) -W -Wall -Wextra
#
#$(info $(LIBS))	
#$(error exit!)
#
FOR_DIST :=              \
		COPYING  \
		include  \
		Makefile \
		ppg      \
		tests    \
		src

O_LIB := $(S_LIB:%.c=%.o)
O_LIB := $(notdir $(O_LIB))
O_LIB := $(addprefix $(LIB_BD)/, $(O_LIB))
D_LIB := $(O_LIB:%.o=%.d)

#DEPFLAGS    = -MT $@ -MMD -MP -MF $(DEPS)/$*.d
#COMPILE.c   = $(CC) $(DEPFLAGS) $(CFLAGS) -c
COMP_LIB.c   = $(CC) -MT $@ -MMD -MP -MF $(LIB_BD)/$*.d $(CFLAGS) -c
COMP_PPG.c   = $(CC) -MT $@ -MMD -MP -MF $(PPG_BD)/$*.d $(CFLAGS) -c

.PHONY:		clean          \
		clean-tests    \
		dist           \
		check          \
		strip          \
		install        \
		a              \
		ppg            \
		ppg-so         \
		ppg-so-install \
		so             \
		so-install     \
		a-install      \
		h-install

ppg:		$(PPG_NAME)

so:		$(SO_NAME)

ppg-so:		$(PPG-W-SO)

$(PPG-W-SO):    $(O_PPG) | so-install
		$(CC) -L $(libdir) -o $@ $^ -lppg $(LIBS)

$(PPG_NAME):	$(O_PPG) $(A_NAME)
		$(CC) $^ -o $@ $(LIBS)

a:		$(A_NAME)

$(A_NAME):	$(O_LIB)
		ar rcs $@ $^

$(SO_NAME):	$(O_LIB)
		$(CC) -shared -o $@ $^

$(LIB_BD)/%.o:	src/%.c $(LIB_BD)/%.d | $(LIB_BD)
		$(COMP_LIB.c) $(OUTPUT_OPTION) $<

$(PPG_BD)/%.o:	ppg/%.c $(PPG_BD)/%.d | $(PPG_BD)
		$(COMP_PPG.c) $(OUTPUT_OPTION) $<

$(LIB_BD):
		@mkdir -p $@

$(PPG_BD):
		@mkdir -p $@

clean:
		-@$(RM) -r $(LIB_BD)
		-@$(RM) -r $(PPG_BD)
		-@$(RM) ppg-$(VERSION).tgz	

strip:		ppg
		@strip $(PPG_NAME)

ppg-so-install:	ppg-so
		$(INSTALL) -s -b -D -v $(PPG-W-SO) $(bindir)/$(notdir $(PPG_NAME))

install:	strip
		$(INSTALL) $(PPG_NAME) $(bindir)

so-install:	so
		$(INSTALL) $(SO_NAME) $(libdir)

a-install:	a
		$(INSTALL) $(A_NAME) $(libdir)

h-install:	
		$(INSTALL) -d $(includedir)/ppg
		$(INSTALL) -Dvm 644 $(IDIR)/ppg/* $(includedir)/ppg

dist:		clean clean-tests
		@mkdir -p ppg-$(VERSION)
		cp -a -t ppg-$(VERSION) $(FOR_DIST)
		tar -zcf ppg-$(VERSION).tgz ppg-$(VERSION)
	        -@$(RM) -r ppg-$(VERSION)

check:
		$(MAKE) -C tests

clean-tests:
		$(MAKE) -C tests clean

$(D_LIB):
include $(wildcard $(D_LIB))

$(D_PPG):
include $(wildcard $(D_PPG))
