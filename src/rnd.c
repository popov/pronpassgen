//////////////////////////////////////////////////////////////////////////
// Copyright (c) 1999, 2000, 2001, 2002  Adel I. Mirzazhanov
// Copyright (c) 2020  Boris Popov <popov@whitekefir.ru>
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  1. Redistributions of source code must retain the above
//     copyright notice, this list of conditions and the
//     following disclaimer.
//
//  2. Redistributions in binary form must reproduce the
//     above copyright notice, this list of conditions and the
//     following disclaimer in the documentation and/or other
//     materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names
//     of its contributors may be used to endorse or promote
//     products derived from this software without specific
//     prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)
#include <strings.h>
#endif
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <ppg/rnd.h>

#ifdef HAVE_OPENSSL
#include <openssl/sha.h>

typedef unsigned char BYTE;
#define SHA_DIGESTSIZE 20
#else  //HAVE_OPENSSL
#include "sha.h"
#endif //HAVE_OPENSSL

UINT32 __rnd_seed[2]; // Random Seed 2*32=64

// ANSI X9.17 pseudorandom generator that uses SHA1 algorithm instead of DES
UINT32 x917sha1_rnd() {
     struct timeval local_time;
     UINT32 I[2] = { 0, 0 };
     UINT32 I_plus_s[2] = { 0, 0 };
     UINT32 Xi[2] = { 0, 0 };
     UINT32 Xi_plus_I[2] = { 0, 0 };
     BYTE hash [SHA_DIGESTSIZE];
     gettimeofday(&local_time, 0);

#ifdef HAVE_OPENSSL
     SHA_CTX sha_ctx;
     SHA1_Init(&sha_ctx);
     SHA1_Update(&sha_ctx, &local_time, sizeof(struct timeval));
     SHA1_Final(hash, &sha_ctx);
#else  //HAVE_OPENSSL
     ppg_SHA_INFO shaInfo;
     ppg_shaInit(&shaInfo);
     ppg_shaUpdate(&shaInfo, (BYTE*) &local_time, sizeof(struct timeval));
     ppg_shaFinal(&shaInfo, hash);
#endif //HAVE_OPENSSL

     memcpy(&I[0], &hash[0], sizeof(I));

     // I0 (+) s0
     I_plus_s[0] = I[0] ^ __rnd_seed[0];

     // I1 (+) s1
     I_plus_s[1] = I[1] ^ __rnd_seed[1];

#ifdef HAVE_OPENSSL
     SHA1_Init(&sha_ctx);
     SHA1_Update(&sha_ctx, &I_plus_s, 8);
     SHA1_Final(hash, &sha_ctx);
#else  //HAVE_OPENSSL
     ppg_shaInit(&shaInfo);
     ppg_shaUpdate(&shaInfo, (BYTE*) &I_plus_s, 8);
     ppg_shaFinal(&shaInfo, hash);
#endif //HAVE_OPENSSL

     // Xi=Ek( I (+) s )
     memcpy(&Xi[0], &hash[0], sizeof(Xi));

     // Xi0 (+) I0
     Xi_plus_I[0] = Xi[0] ^ I[0];

     // Xi1 (+) I1
     Xi_plus_I[1] = Xi[1] ^ I[1];

#ifdef HAVE_OPENSSL
     SHA1_Init(&sha_ctx);
     SHA1_Update(&sha_ctx, &Xi_plus_I, 8);
     SHA1_Final(hash, &sha_ctx);
#else  //HAVE_OPENSSL
     ppg_shaInit(&shaInfo);
     ppg_shaUpdate(&shaInfo, (BYTE*) &Xi_plus_I, 8);
     ppg_shaFinal(&shaInfo, hash);
#endif //HAVE_OPENSSL

     // s=Ek( Xi (+) I )
     memcpy(&__rnd_seed[0], &hash[0], sizeof(__rnd_seed));
     return (Xi[0]);
}

// Initializes seed
void x917_setseed(UINT32 seed, int quiet) {
     int fd;
     UINT32 drs[2];
     UINT32 pid = getpid();

     if ((fd = open(PPG_DEVRANDOM, O_RDONLY)) != -1) {
	  read(fd, &drs[0], 8);
	  __rnd_seed[0] = seed ^ drs[0];
	  __rnd_seed[1] = seed ^ drs[1];
	  close(fd);

     } else if ((fd = open(PPG_DEVURANDOM, O_RDONLY)) != -1) {
	  read(fd, &drs[0], 8);
	  __rnd_seed[0] = seed ^ drs[0];
	  __rnd_seed[1] = seed ^ drs[1];
	  close(fd);

     } else {
#if !defined(WIN32) && !defined(_WIN32) && !defined(__WIN32) && !defined(__WIN32__)
	  if (quiet != TRUE) {
	       fprintf(stderr, "CAN NOT USE RANDOM DEVICE TO GENERATE RANDOM SEED\n");
	       fprintf(stderr, "USING LOCAL TIME AND PID FOR SEED GENERATION !!!\n");
	       fflush(stderr);
	  }
#endif /* WIN32 */
	  __rnd_seed[0] = seed ^ pid;
	  __rnd_seed[1] = seed ^ pid;
     }
}

// Produces a Random number from 0 to n-1.
UINT randint(int n) {
     return (x917sha1_rnd() % n);
}
